<?php
use \userlib\User;
use \userlib\minishop\Cart;
use \userlib\minishop\User as LibUser;

//-- autoload
spl_autoload_register('includeClass');
function includeClass($className){
    $path = str_replace('\\','/',$className).'.php';
    if(file_exists($path)){
        require_once $path;
    }
}
echo User::sayHello();
echo '<br>';
echo Cart::getCart();
echo '<br>';
echo \userlib\minishop\User::sayHello();
echo '<br>';
echo LibUser::sayHello();
echo '<br>';
echo \userlib\minishop\Admin::sayHello();


